﻿
﻿
#include"Huffman.h"
/*
* TEAM - PROJECT : HUFFMAN - G4
* SUBJECT : DATA STRUCTURES AND ALGORITHMS
* GROUP - 2
*/

void Huffman::Khoitao(){
	for (int i = 0; i < MAX_NODE; i++) {
		huffTree[i].c = i;
		huffTree[i].freq = 0;
		huffTree[i].used = false;
		huffTree[i].nLeft = -1;
		huffTree[i].nRight = -1;
	}
	soLuongFile = 0;
	tuyChonSoFile = false;
}

unsigned int Huffman::ThongKeTanSoXuatHien(const char* tenFile){
	ifstream fi(tenFile, ios::binary);
	unsigned char c;
	unsigned int byte = 0;
	while (1)	
	{	
		fi.read((char*)&c, 1);
		if (fi.eof()) {
			break;
		}
		byte++;
		huffTree[c].freq++;
	}
	fi.close();
	return byte;
}

void Huffman::XuatBangThongKe(){
	printf("Bang thong ke tan suat: \n");
	for (int i = 0; i < 256; i++) {
		if (huffTree[i].freq > 0) {
			printf("%c: %d\n", i, huffTree[i].freq);
		}
	}
}

bool  Huffman::Tim2PhanTuMin(int &x, int &y, int nNode){
	x = y = -1;
	for (int i = 0; i < nNode; i++){
		if (huffTree[i].freq > 0 && huffTree[i].used == false){
			if (x == -1)
				x = i;
			else if (y == -1){
				y = i;
				if (huffTree[x].freq > huffTree[y].freq || (huffTree[x].freq == huffTree[y].freq && x > y))
					swap(x, y);
			}
			else if(huffTree[i].freq <= huffTree[y].freq){
				if (huffTree[i].freq < huffTree[y].freq || (huffTree[i].freq == huffTree[y].freq && i < y))
					y = i;
				if (huffTree[y].freq < huffTree[x].freq || (huffTree[x].freq == huffTree[y].freq && y < x))
					swap(x, y);
			}
		}
	}
	if (y == -1)
		return false;
	return true;
}
int  Huffman::TaoCayHuffman(){
	int nNode = 256;
	int i, j;
	bool timThay = false;
	while (true)	{
		timThay = Tim2PhanTuMin(i, j, nNode);
		if (!timThay){
			break;
		}

		huffTree[nNode].c = (unsigned char)i;// (huffTree[i].c < huffTree[j].c) ? huffTree[i].c : huffTree[j].c;
		huffTree[nNode].freq = huffTree[i].freq + huffTree[j].freq;
		huffTree[nNode].nLeft = i;
		huffTree[nNode].nRight = j;

		huffTree[i].used = true;
		huffTree[j].used = true;
		
		nNode++;
	}
	Root = nNode - 1;
	return Root;

}

void Huffman::XuatCayHuffman(int node, int tab){
	if (node == -1) {
		return;
	}
	for (int i = 0; i < tab; i++) {
		printf("\t");
	}
	if (huffTree[node].nLeft == -1 && huffTree[node].nRight == -1)	{
		printf("%c\n", huffTree[node].c);
	}
	else	{
		printf("%c..\n", huffTree[node].c);
		XuatCayHuffman(huffTree[node].nLeft, tab - 1);
		XuatCayHuffman(huffTree[node].nRight, tab + 1);
	}
}
void Huffman::DuyetCayHuffman(int node, char maBit[], int nMaBit){
	if (node == -1) {
		return;
	}
	//cout << huffTree[node].c << "(" <<  (int)huffTree[node].c << ") ";
	if (huffTree[node].nLeft == -1 && huffTree[node].nRight == -1){
		bangMaBit[node].soBit = nMaBit;
		bangMaBit[node].bits = new char[nMaBit];
		for (int i = 0; i < nMaBit; i++) {
			bangMaBit[node].bits[i] = maBit[i];
		}
		return;
	}
	else {
		maBit[nMaBit] = 0;
		DuyetCayHuffman(huffTree[node].nLeft, maBit, nMaBit + 1);
		maBit[nMaBit] = 1;
		DuyetCayHuffman(huffTree[node].nRight, maBit, nMaBit + 1);
	}
}
void Huffman::PhatSinhMaBit() {
	for (int i = 0; i < 256; i++) {
		bangMaBit[i].soBit = 0;
		bangMaBit[i].bits = NULL;
	};
	char maBit[MAX_BIT_LEN / 8];
	int nMaBit = 0;

	DuyetCayHuffman(Root, maBit, nMaBit);
}

void Huffman::XuatBangMaBit() {
	for (int i = 0; i < 256; i++)
	if (bangMaBit[i].soBit > 0)	{
		printf("%c: ", i);
		for (int j = 0; j < bangMaBit[i].soBit; j++)
			printf("%d", bangMaBit[i].bits[j]);
		printf("\n");
	}
}

void Huffman::MaHoa1KyTu(unsigned char c, unsigned char &out, unsigned char &viTriBit, string &BIN) {

	for (int i = 0; i < bangMaBit[c].soBit; i++) {
		if (bangMaBit[c].bits[i] == 1) {
			out = out | (1 << viTriBit); 
		}
		if (viTriBit == 0){
			viTriBit = 7;
			for (int u = 7; u >= 0; u--)
				if ((out >> u) & 1)
					BIN.insert(BIN.size(), "1");
				else
					BIN.insert(BIN.size(), "0");
			out = 0;
		}
		else {
			viTriBit--;
		}
	}
}

string Huffman::LayDayBits(const char* file_goc){
	//Thay the cac ky tu bang ma bit tuong ung
	string BIN; //Chuỗi dãy BIN
	unsigned char out = 0;				// ky tu se xuat ra
	unsigned char soBitCoNghia = 0;		// byte cuoi co the ko su dung het cac bit nen can luu so bit co nghia cua byte cuoi

	unsigned char c;
	unsigned char viTriBit = 7;	
	ifstream fi(file_goc, ios::binary);
	while (true){
		fi.read((char*)&c, 1);
		if (fi.eof()) {
			break;
		}
		MaHoa1KyTu(c, out, viTriBit, BIN);
	}
	soBitCoNghia = 7 - viTriBit;
	if (soBitCoNghia == 0) {
		soBitCoNghia = 8;
	}
	else {
		for (int i = 7; i >= 7 - soBitCoNghia + 1; i--)
		if ((out >> i) & 1)
			BIN.insert(BIN.size(), "1");
		else
			BIN.insert(BIN.size(), "0");
	}
	fi.close();
	return BIN;
}

void Huffman::ghiBangTanSo(ofstream &file_nen){
	unsigned char c;
	for (int i = 0; i < 256; i++){
		if (huffTree[i].freq > 0){
			c = (unsigned char)i;
			file_nen.write((char*)&c,1);
			file_nen.write((char*)&huffTree[i].freq, sizeof(huffTree[i].freq));
		}
	}
	file_nen.write("#",1);
	int x = -1;
	file_nen.write((char*)&x,sizeof(x));
}

void Huffman::ghiDayBits(string &BIN,ofstream &fnen,int VitriFile){
	int soBitdu = (8 - BIN.size() % 8) % 8;
	BIN.insert(BIN.size(), soBitdu, '0');
	unsigned char c = 0;
	DanhSachFile[VitriFile].vitri = fnen.tellp();
	int u = 7, sizeBIN = BIN.size();
	for (int i = 1; i <= sizeBIN; i++){
		c += pow(2, u--)*(BIN[i - 1] - 48);
		if (i % 8 == 0)
			u = 7, fnen.write((char*)&c, 1), c = 0;
	}
	DanhSachFile[VitriFile].soByte = BIN.size() / 8;
	DanhSachFile[VitriFile].soBitDu = soBitdu;
}

void Huffman::LayDanhSachTenFile(string folderlink){
	vector<string> list;
	DIR *pDIR;
	struct dirent *entry;
	string str;
	if (pDIR = opendir(folderlink.c_str())){
		while (entry = readdir(pDIR)){
			if (strcmp(entry->d_name, ".") != 0 && strcmp(entry->d_name, "..") != 0)
			{
				str = entry->d_name;
				if (str.find('.') != string::npos)
				{
					list.push_back(str);
				}
			}
		}
		closedir(pDIR);
	}
	soLuongFile = list.size();
	DanhSachFile.resize(soLuongFile);
	for (unsigned int i = 0; i < soLuongFile; i++){
		int dot = list[i].rfind('.');
		strcat(DanhSachFile[i].F.ten, list[i].substr(0, dot).c_str());
		strcat(DanhSachFile[i].F.dinhdang, list[i].substr(dot).c_str());
	}
}

void Huffman::NenHuffmanFolder(string foderlink, string Name_fnen){
	if (foderlink[foderlink.size() - 1] == '\\')
		foderlink.erase(foderlink.size() - 1, 1);
	Khoitao();
	cout << "Lay danh sach file....\n";
	LayDanhSachTenFile(foderlink);
	//CapNhatDS_File();
	foderlink.insert(foderlink.size(), "\\");
	vector<string> LinkFile; LinkFile.resize(soLuongFile);
	for (unsigned int i = 0; i < soLuongFile; i++){
		LinkFile[i] = foderlink;
		LinkFile[i].insert(foderlink.size(), DanhSachFile[i].F.ten);
		LinkFile[i].insert(LinkFile[i].size(), DanhSachFile[i].F.dinhdang);
		DanhSachFile[i].F.kichthuoc = ThongKeTanSoXuatHien(LinkFile[i].c_str());
	}
	cout << "Tao cay huffman...\n";
	TaoCayHuffman();
	cout << "Phat sinh ma Bit...\n";
	PhatSinhMaBit();
	ofstream fnen(Name_fnen, ios::binary);
	fnen.write("SHuff_G02",9);	//Ma nhan dien file nen Static Huffman
	fnen.write((char*)&sizeFnen, sizeof(sizeFnen));
	fnen.seekp(sizeof(File_nen)*soLuongFile + sizeof(unsigned int), ios::cur);

	cout << " ==>> Nen: \n";
	ghiBangTanSo(fnen);
	for (unsigned int i = 0; i < soLuongFile; i++){
		cout <<"  " << i + 1 << ". " << DanhSachFile[i].F.ten << DanhSachFile[i].F.dinhdang <<"...." << endl;
		string BIN = LayDayBits(LinkFile[i].c_str());
		ghiDayBits(BIN,fnen,i);
	}
	cout << "  100%" << endl;
	sizeFnen = fnen.tellp();
	fnen.seekp(9, ios::beg);
	fnen.write((char*)&sizeFnen, sizeof(sizeFnen));
	ghiDanhSach_File(fnen);
	fnen.close();
}

void Huffman::ghiDanhSach_File(ofstream& fnen){
	fnen.write((char*)&soLuongFile, sizeof(soLuongFile));
	for (int i = 0; i < soLuongFile; i++)
		fnen.write((char*)&DanhSachFile[i], sizeof(File_nen));
}

void Huffman::giaiNenFile(ifstream& fnen,string Thumuc_giainen, int vitriFile){
	if (tuyChonSoFile)
		fnen.seekg(DanhSachFile[vitriFile].vitri, ios::beg);
	string tenFile = Thumuc_giainen;
	tenFile.insert(tenFile.size(), DanhSachFile[vitriFile].F.ten);
	tenFile.insert(tenFile.size(), DanhSachFile[vitriFile].F.dinhdang);
	ofstream fGiainen(tenFile, ios::binary);
	int pCurr = Root;
	unsigned int soByte = DanhSachFile[vitriFile].soByte;
	unsigned char c;
	while (soByte-- > 0){
		int i = 0, n = 0;
		fnen.read((char *)&c, 1);
		if (soByte == 0)
			i = DanhSachFile[vitriFile].soBitDu;
		while (i++ < 8){
			n = (c & 0x80) >> 7;
			c = c << 1;
			pCurr = n == 0 ? huffTree[pCurr].nLeft : huffTree[pCurr].nRight;
			if (huffTree[pCurr].nLeft == -1 && huffTree[pCurr].nRight == -1){
				fGiainen.write((char *)&huffTree[pCurr].c, 1);
				pCurr = Root;
			}
		}

	}
	//Cheksum....
	if (DanhSachFile[vitriFile].F.kichthuoc != fGiainen.tellp())
		cout << "\tGiai nen " << DanhSachFile[vitriFile].F.ten << DanhSachFile[vitriFile].F.dinhdang << " khong thanh cong!\n";
	fGiainen.close();
}

void Huffman::GiaiNenFolder(string Name_fnen, string Thumuc_giainen, int *vitriFile, int n){
	if (Thumuc_giainen == "")
	{
		int v = Name_fnen.rfind('\\');
		if (v != -1)
		Thumuc_giainen = Name_fnen.substr(0, v);
	}
	else if (Thumuc_giainen[Thumuc_giainen.size() - 1] != '\\')
		Thumuc_giainen.insert(Thumuc_giainen.size(), "\\");
	ifstream fnen(Name_fnen, ios::binary);
	if (!fnen){
		cout << "Khong tim thay file!\n";
		system("pause");
		exit(0);
	}
	char ID[10];
	fnen.read(ID, 9);
	ID[9] = '\0';
	if (strcmp(ID, "SHuff_G02") != 0){
		cout << "File nen khong hop le!\n";
		system("pause");
		exit(0);
	}
	Khoitao();
	fnen.read((char*)&sizeFnen, sizeof(sizeFnen));
	fnen.read((char*)&soLuongFile, sizeof(soLuongFile));
	DanhSachFile.resize(soLuongFile);
	for (int i = 0; i < soLuongFile; i++)
		fnen.read((char*)&DanhSachFile[i], sizeof(File_nen));
	
	if (vitriFile != NULL)
		CapNhatDS_File2(vitriFile, n);
	unsigned char c;
	unsigned int num;
	while (1){
		fnen.read((char*)&c, 1);
		fnen.read((char*)&num, sizeof(num));
		if (num == -1)
			break;
		huffTree[c].freq = num;
	}
	//cout << "Tao cay huffman...\n";
	TaoCayHuffman();
	//XuatCayHuffman(Root,5);
	//CapNhatDS_File();

	cout << " ==>> Giai Nen '" << Name_fnen << "':\n";
	for (int k = 0; k < soLuongFile; k++){
		cout << "  " << k + 1 << ". " << DanhSachFile[k].F.ten << DanhSachFile[k].F.dinhdang << "..." << endl;
		giaiNenFile(fnen, Thumuc_giainen, k);
	}
	cout << "  100%" << endl;
	fnen.close();
}

void Huffman::XuatDanhSachFile(int tab){
	string TAB;
	while (tab-- >= 0)
		TAB.insert(TAB.size(), "  ");
	for (int i = 0; i < soLuongFile; i++){
		cout << TAB << i + 1 << ". " << DanhSachFile[i].F.ten << DanhSachFile[i].F.dinhdang << endl;
	}
}
void Huffman::XuatThongTin_DanhSachFile(){
	long long ktgoc = 0;
	cout << "Ten tap tin\t\t\t\t" << setw(10) << "Goc (byte)\tNen (Byte)\n";//   Hieu suat\n";
	for (int i = 0; i < soLuongFile; i++){
		string name = DanhSachFile[i].F.ten; int size = name.size();
		name.insert(name.size(), DanhSachFile[i].F.dinhdang);
		
		cout << name << setfill('.') << setw(50 - name.size());
		cout << DanhSachFile[i].F.kichthuoc << setw(12) << DanhSachFile[i].soByte << endl;
		//cout << roundf((100 - roundf((100.0*DanhSachFile[i].soByte / DanhSachFile[i].F.kichthuoc) * 10) / 10) * 10 / 10) << "%" << endl;
		ktgoc += DanhSachFile[i].F.kichthuoc;
	}
	cout << setfill(' ');
	cout << "\nTong:" << setw(45) << roundf((ktgoc / 1024.0) * 10 / 10) << " KB" << setw(9) << roundf((sizeFnen / 1024.0) * 10 / 10) << " KB\n";
	cout << "Hieu suat: "   << roundf((100 - roundf(100.0 * sizeFnen / ktgoc) * 10 / 10) * 10 / 10) << "%" << endl;
}
void Huffman::LayThongTinFileNen(string Name_fnen){
	ifstream fnen(Name_fnen, ios::binary);
	if (!fnen){
		cout << "Khong tim thay file!\n";
		system("pause");
		exit(0);
	}
	char ID[10];
	fnen.read(ID, 9);
	ID[9] = '\0';
	if (strcmp(ID, "SHuff_G02") != 0){
		cout << "File nen khong hop le!\n";
		system("pause");
		exit(0);
	}
	Khoitao();
	fnen.read((char*)&sizeFnen, sizeof(sizeFnen));
	fnen.read((char*)&soLuongFile, sizeof(soLuongFile));
	DanhSachFile.resize(soLuongFile);
	for (int i = 0; i < soLuongFile; i++)
		fnen.read((char*)&DanhSachFile[i], sizeof(File_nen));
	fnen.close();
}

bool Huffman::CapNhatDS_File2(int *vitriFile,int n){
	tuyChonSoFile = true;
	bool *BangChon = new bool[soLuongFile];
	for (int i = 0; i < soLuongFile; i++){
		BangChon[i] = false;
	}
	for (int u = 0; u < n; u++){
		if (vitriFile[u] <= 0 || vitriFile[u] > soLuongFile){
			cout << "File so " << vitriFile[u] << " khong ton tai!\n";
			system("pause"); exit(0);
		}
		BangChon[vitriFile[u] - 1] = true;
	}
	for (int i = 0, j = 0; j < soLuongFile; j++, i++)
	if (!BangChon[i]){
		DanhSachFile.erase(DanhSachFile.begin() + j, DanhSachFile.begin() + j + 1);
		soLuongFile--;
		j--;
	}
	delete[]BangChon;
	return true;
}
void Huffman::CapNhatDS_File(){
	//cout << "Danh sach file:\n";
	int is, soLuong = soLuongFile, vitri;
	bool IsSelec;
	XuatDanhSachFile(1);
	cout << "Nhap (0/x/-1) <=> (Chon tat ca/vi tri file/ket thuc)\n";
	cin >> vitri;
	if (vitri == 0)
		return;
	bool *BangChon = new bool[soLuongFile];
	for (int i = 0; i < soLuongFile; i++){
		BangChon[i] = false;
	}
	while (1){
		if (vitri == -1)
			break;
		if (vitri > soLuongFile || vitri <= 0){
			cout << "File " << vitri << " khong ton tai! Xin nhap lai: ";
			cin >> vitri;
			continue;
		}
		BangChon[vitri - 1] = true;
		cin >> vitri;
	}
	for (int i = 0,j = 0; j < soLuongFile;j++,i++)
		if (!BangChon[i]){
			DanhSachFile.erase(DanhSachFile.begin() + j, DanhSachFile.begin() + j + 1);
			soLuongFile--;
			j--;
		}
	delete[]BangChon;
	tuyChonSoFile = true;
}

