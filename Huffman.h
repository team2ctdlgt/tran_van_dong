#pragma once
#include <string>
#include<fstream>
#include<vector>
#include <dirent.h>
#include <iomanip>
#include"ThongTin_File.h"


#define MAX_NODE 256*2 - 1
#define MAX_BIT_LEN  10000
struct NODE {
	unsigned char	c;		// ky tu
	unsigned int freq;	// so lan xuat hien
	bool	used;	// danh dau node da xu ly chua 
	int		nLeft;	// chi so nut con nam ben trai
	int		nRight;	// chi so nut con nam ben phai
};
struct MABIT {
	char* bits;
	int	soBit;
};
class Huffman{
	NODE huffTree[MAX_NODE];
	MABIT bangMaBit[256];
	int Root;
	unsigned int soLuongFile;
	vector<File_nen> DanhSachFile;
	bool tuyChonSoFile;
	unsigned int sizeFnen;
public:
	void Khoitao();
	unsigned int ThongKeTanSoXuatHien(const char* tenFile);
	bool Tim2PhanTuMin(int &i, int &j, int nNode);
	int TaoCayHuffman();
	void DuyetCayHuffman(int node, char maBit[], int nMaBit);
	void PhatSinhMaBit();
	void MaHoa1KyTu(unsigned char c, unsigned char &out, unsigned char &viTriBit, string &BIN);

	void XuatBangThongKe();
	void XuatCayHuffman(int node, int tab);
	void XuatBangMaBit();
	
	string LayDayBits(const char* file_goc);
	void LayDanhSachTenFile(string folderlink);

	void ghiBangTanSo(ofstream &file_nen);
	void ghiDayBits(string &BIN, ofstream &fnen, int VitriFile);
	void ghiDanhSach_File(ofstream& fnen);

	void NenHuffmanFolder(string foderlink,string Name_fnen);

	void giaiNenFile(ifstream& fnen, string Thumuc_giainen, int vitriFile);
	void GiaiNenFolder(string fnen = "", string Thumuc_giainen = "", int *vitriFile = NULL, int n = 0);

	void XuatDanhSachFile(int tab = 0);
	void XuatThongTin_DanhSachFile();

	void LayThongTinFileNen(string Name_fnen);

	void CapNhatDS_File();
	bool CapNhatDS_File2(int *vitriFile, int n);
};