﻿#include"Huffman.h"

void TachSo(int *&DSfile, int &n, char* argv){
	vector<int> ds;
	char *p = strtok(argv, "-,[] ");

	cout << "Danh sach file giai nen: ";
	while (p != NULL)
	{
		cout << p << "  ";
		ds.push_back(atoi(p));
		p = strtok(NULL, "-,[] ");
	}
	cout << endl << endl;
	n = ds.size();
	DSfile = new int[n];
	for (int i = 0; i < n; i++)
		DSfile[i] = ds[i];
	delete[]p;
}
void main(int argc,char** argv){
	if (strcmp(argv[1], "G4") != 0){
		cout << "Sai cu phap!\n";
		return;
	}
	if (strcmp(argv[2], "-e") == 0){
		Huffman NEN;
		NEN.NenHuffmanFolder(argv[3], argv[4]);
		return;
	}
	if (strcmp(argv[2], "-v") == 0){
		Huffman XemTT;
		XemTT.LayThongTinFileNen(argv[3]);
		XemTT.XuatThongTin_DanhSachFile();
		return;
	}
	if (strcmp(argv[2], "-d") == 0){
		Huffman GiaiNen;
		if (strcmp(argv[3], "-all") != 0){
			GiaiNen.LayThongTinFileNen(argv[4]);
			GiaiNen.XuatDanhSachFile(1);
			int *DSfile, soluong;
			TachSo(DSfile, soluong, argv[3]);
			GiaiNen.GiaiNenFolder(argv[4], "", DSfile, soluong);
			delete[]DSfile;
		}
		else
			GiaiNen.GiaiNenFolder(argv[4]);
	}
}